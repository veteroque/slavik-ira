import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { IraSidebarComponent } from './sidebar.component'

describe('SidebarComponent', () => {
  let component: IraSidebarComponent
  let fixture: ComponentFixture<IraSidebarComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IraSidebarComponent ],
    })
    .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(IraSidebarComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
