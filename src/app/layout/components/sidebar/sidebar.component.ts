import { Component, Input } from '@angular/core'
import { SidebarItem } from '../sidebar-item/SidebarItem.interface'

@Component({
  selector: 'ira-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class IraSidebarComponent {

  @Input()
  items: SidebarItem[] = []



}
