import { Component } from '@angular/core'
import { Step, StepStatus } from '../stepper/stepper-model.interface'

@Component({
  selector: 'ira-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
})
export class IraToolbarComponent {

  steps: Step[] = [
    {
      icon: 'document1',
      index: 0,
      label: 'IRA Application',
      value: 'step1',
      status: StepStatus.Active,
    },
    {
      icon: 'document2',
      index: 1,
      label: 'Risk Assessment',
      value: 'step2',
      status: StepStatus.Inactive,
    },
    {
      icon: 'document3',
      index: 2,
      label: 'Upload ID',
      value: 'step3',
      status: StepStatus.Inactive,
    },
  ]


}
