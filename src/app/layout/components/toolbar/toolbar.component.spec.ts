import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { IraToolbarComponent } from './toolbar.component'

describe('ToolbarComponent', () => {
  let component: IraToolbarComponent
  let fixture: ComponentFixture<IraToolbarComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IraToolbarComponent ],
    })
    .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(IraToolbarComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
