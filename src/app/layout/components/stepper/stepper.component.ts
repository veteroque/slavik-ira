import { Component, Input } from '@angular/core'
import { Step } from './stepper-model.interface'

@Component({
  selector: 'ira-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss'],
})
export class StepperComponent  {

  @Input()
  steps: Step[] = []

}
