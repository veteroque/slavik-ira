export interface Step {
    index: number
    value: string
    icon: string
    label: string
    status: StepStatus
}

export enum StepStatus {
    Active = 'ACTIVE',
    Inactive = 'INACTIVE',
}
