import { Component, Input } from '@angular/core'
import { Step } from '../stepper/stepper-model.interface'

@Component({
  selector: 'ira-stepper-item',
  templateUrl: './stepper-item.component.html',
  styleUrls: ['./stepper-item.component.scss'],
})
export class StepperItemComponent  {

  @Input()
  step: Step

}
