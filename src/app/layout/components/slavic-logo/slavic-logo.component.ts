import { Component, OnInit } from '@angular/core'

@Component({
  selector: 'ira-slavic-logo',
  templateUrl: './slavic-logo.component.html',
  styleUrls: ['./slavic-logo.component.scss'],
})
export class IraSlavicLogoComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
