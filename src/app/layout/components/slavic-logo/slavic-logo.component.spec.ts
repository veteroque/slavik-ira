import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { IraSlavicLogoComponent } from './slavic-logo.component'


describe('SlavicLogoComponent', () => {
  let component: IraSlavicLogoComponent
  let fixture: ComponentFixture<IraSlavicLogoComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IraSlavicLogoComponent ],
    })
    .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(IraSlavicLogoComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
