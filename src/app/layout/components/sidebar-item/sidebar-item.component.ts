import { Component, Input } from '@angular/core'
import { SidebarItem } from './SidebarItem.interface'

@Component({
  selector: 'ira-sidebar-item',
  templateUrl: './sidebar-item.component.html',
  styleUrls: ['./sidebar-item.component.scss'],
})
export class IraSidebarItemComponent {

  @Input()
  item: SidebarItem

}
