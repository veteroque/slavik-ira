import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { IraSidebarItemComponent } from './sidebar-item.component'

describe('SidebarItemComponent', () => {
  let component: IraSidebarItemComponent
  let fixture: ComponentFixture<IraSidebarItemComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IraSidebarItemComponent ],
    })
    .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(IraSidebarItemComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
