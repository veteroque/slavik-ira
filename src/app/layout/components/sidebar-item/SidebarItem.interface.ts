export interface SidebarItem {
    state: SidebarItemState,
    label: string,
    value?: string
}

export enum SidebarItemState {
    Active = 'ACTIVE',
    Complete = 'COMPLETE',
    Incomplete = 'INCOMPLETE',
}
