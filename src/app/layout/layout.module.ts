import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { IraSidebarItemComponent } from './components/sidebar-item/sidebar-item.component'
import { IraSidebarComponent } from './components/sidebar/sidebar.component'
import { IraSlavicLogoComponent } from './components/slavic-logo/slavic-logo.component'
import { IraToolbarComponent } from './components/toolbar/toolbar.component'
import { StepperComponent } from './components/stepper/stepper.component'
import { StepperItemComponent } from './components/stepper-item/stepper-item.component'



@NgModule({
  declarations: [
    IraToolbarComponent,
    IraSidebarComponent,
    IraSidebarItemComponent,
    IraSlavicLogoComponent,
    StepperComponent,
    StepperItemComponent,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    IraToolbarComponent,
    IraSidebarComponent,
    IraSidebarItemComponent,
    IraSlavicLogoComponent,
  ],
})
export class LayoutModule { }
