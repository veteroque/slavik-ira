import { DatePipe } from '@angular/common'
import { Component, OnInit } from '@angular/core'
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms'
import { IraSelectOption, relationshipSelectDefaultOpts, ssnSelectDefaultOpts } from './apllication-form-defaults'

@Component({
  selector: 'ira-application',
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.scss'],
  providers: [
    DatePipe,
  ],
})
export class ApplicationComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    private datePipe: DatePipe,
    ) { }

  totalSubmitted = 0

  parseInt = parseInt

  get beneficiariesArray() {
    return this.beneficiariesForm.get('beneficiariesArray') as FormArray
  }

  get contingentCheckbox() {
    return this.beneficiariesForm.get('contingentBeneficiaries') as FormControl
  }

  beneficiariesForm: FormGroup

  relationshipOpts: IraSelectOption[] = relationshipSelectDefaultOpts
  ssnOpts: IraSelectOption[] = ssnSelectDefaultOpts

  ngOnInit() {
    this.beneficiariesForm = this.formBuilder.group({

      beneficiariesArray: this.formBuilder.array([

        new FormGroup({
          fullName: new FormControl('Tom Cruise', [Validators.minLength(3)]),
          dateOfBirth: new FormControl(this.datePipe.transform(new Date(1980, 5, 26), 'yyyy-MM-dd')),
          ssn: new FormControl('SSN'),
          optional: new FormControl(''),
          relationship: new FormControl('Trust'),
          percentage: new FormControl('', [Validators.required, Validators.min(0), Validators.max(100)]),
        }),
      ]),
      contingentBeneficiaries: this.formBuilder.control(
        true,
      ),
    })

    this.beneficiariesForm.valueChanges.subscribe(v => console.log(v)    )
  }

  addItem() {


    if (this.beneficiariesArray.valid) {

      this.beneficiariesArray.push(
        this.formBuilder.group({
          fullName: new FormControl('Elon Musk', [Validators.minLength(3)]),
          dateOfBirth: new FormControl(this.datePipe.transform(new Date(1980, 5, 26), 'yyyy-MM-dd')),
          ssn: new FormControl('1234'),
          optional: new FormControl(''),
          relationship: new FormControl('Trust'),
          percentage: new FormControl('', [Validators.required, Validators.min(0), Validators.max(100 - this.total)]),
        }),
        )
      }
    this.triggerValidation()

  }

  triggerValidation() {
    // this.beneficiariesArray.get('percentage').markAsDirty()
    this.beneficiariesArray.updateValueAndValidity()
  }

  removeItem(i) {
    this.beneficiariesArray.removeAt(i)

    // Update max validator of the last row
    this.beneficiariesArray.controls.map(

      row => row.get('percentage').setValidators([ Validators.required, Validators.min(0), Validators.max(100 - this.total)]),
    )

    this.beneficiariesArray.updateValueAndValidity()


  }

  get total(): number {
    return this.beneficiariesArray.controls.map(
      fg => fg.get('percentage').valid ? fg.get('percentage').value : 0,
    ).reduce((a, b) => a + b, 0)
  }

}
