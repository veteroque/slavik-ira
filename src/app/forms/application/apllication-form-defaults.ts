
export interface IraSelectOption {
    label: string
    value: string
}

export const relationshipSelectDefaultOpts: IraSelectOption[] = [
    {
        label: 'Trust',
        value: 'value1',
    },
    {
        label: 'Relative',
        value: 'value2',
    },
    {
        label: 'Married',
        value: 'value3',
    },

]

export const ssnSelectDefaultOpts: IraSelectOption[] = [
    {
        label: 'SSN',
        value: 'value1',
    },
    {
        label: 'SSN1',
        value: 'value2',
    },
    {
        label: 'SSN2',
        value: 'value3',
    },

]
