import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { ApplicationComponent } from './application/application.component'
import { BeneficiaryComponent } from './beneficiary/beneficiary.component'
import { TotalLabelComponent } from './total-label/total-label.component'



@NgModule({
  declarations: [
    ApplicationComponent,
    BeneficiaryComponent,
    TotalLabelComponent,
  ],
  exports: [
    ApplicationComponent,
    BeneficiaryComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class IraFormsModule { }
