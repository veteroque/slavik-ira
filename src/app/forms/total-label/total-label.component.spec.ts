import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { TotalLabelComponent } from './total-label.component'

describe('TotalLabelComponent', () => {
  let component: TotalLabelComponent
  let fixture: ComponentFixture<TotalLabelComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TotalLabelComponent ],
    })
    .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalLabelComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
