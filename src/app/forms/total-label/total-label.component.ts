import { Component, Input } from '@angular/core'

@Component({
  selector: 'ira-total-label',
  templateUrl: './total-label.component.html',
  styleUrls: ['./total-label.component.scss'],
})
export class TotalLabelComponent {

  @Input()
  count = 0

  parseInt = parseInt
}
