import { Component } from '@angular/core'
import { SidebarItem, SidebarItemState } from './layout/components/sidebar-item/SidebarItem.interface'

@Component({
  selector: 'ira-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'slavik-ira'

  sidebarItems: SidebarItem[] = [
    {
      label: 'Personal information',
      state: SidebarItemState.Complete,
    },
    {
      label: 'Contact information',
      state: SidebarItemState.Complete,
    },
    {
      label: 'Relationship',
      state: SidebarItemState.Complete,
    },
    {
      label: 'Beneficiaries',
      state: SidebarItemState.Active,
    },
    {
      label: 'Account characteristics',
      state: SidebarItemState.Incomplete,
    },
    {
      label: 'Review',
      state: SidebarItemState.Incomplete,
    },
  ]
}
