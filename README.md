# SlavikIra

## Overview
This project is a showcase of prefferred approaches in app architecture and development process with small one page example. Codebase must be as much simplistic as possible.

## Demo 
Production environment - [https://slavik-ira.now.sh](https://slavik-ira.now.sh)

## Features roadmap
 - [x] auto linting;
 - [x] git hooks;
 - [ ] separate environments for prod, staging, etc;
 - [ ] layout components module;
 - [ ] form components module;
 - [ ] TBD.. ;